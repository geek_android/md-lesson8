package com.martin.gallery;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.drawer_layout2)
    DrawerLayout drawer;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.ThemeStandart_Green);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        initFab();
        initToolbarAndNavDrawer();
        initChooseColorFragment();
    }

    private void initChooseColorFragment() {
        ChooseColorFragment chooseColorFragment = new ChooseColorFragment();
        getSupportFragmentManager().beginTransaction().
                replace(R.id.container_for_fragment, chooseColorFragment).commit();
    }

    private void initToolbarAndNavDrawer() {
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(getNavItemListener());
    }

    @NonNull
    private NavigationView.OnNavigationItemSelectedListener getNavItemListener() {
        return new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                Fragment fragment;

                switch (id) {
                    case R.id.nav_color_change:
                        fragment = new ChooseColorFragment();
                        break;
                    case R.id.nav_home:
                    default:
                        fragment = new MainFragment();
                        break;
                }
                drawer.closeDrawer(GravityCompat.START);
                getSupportFragmentManager().beginTransaction().
                        replace(R.id.container_for_fragment, fragment).commit();
                return true;
            }
        };
    }

    private void initFab() {
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

}
